# Sloth

![alt tag](http://kids.nationalgeographic.com/content/dam/kids/photos/animals/Mammals/Q-Z/sloth-beach-upside-down.jpg.adapt.945.1.jpg)
![alt tag](https://pp.userapi.com/c836721/v836721779/34840/r9KTBsoLYM0.jpg)

Sloth is an application for testing REST API's. Howewer, it can be expanded easily, so you can use plugins to customize your app or add functionality.
For example, exporting project to .MD documentation or to other applications like [Insomnia](https://insomnia.rest)

## Install 

**Sloth** is currently in alpha version, so there is no bundled application. 
So, to get it running, you need to clone this repository

**`git clone https://github.com/enchanting4/Sloth.git`**
**`cd Sloth`**

Then, run **`npm install`** to install all needed packages

And to start **Sloth**, run **`npm start`** and enjoy it( or something else)

## ELP!
If you want to contribute, thanks and welcome, very appreciated. Start issues and etc
