
import Component from './Component'

export default class LayoutManager{
    constructor(){
        this.stateSaveKey = 'appLayoutState';
        this.componentName = 'app-layout';
        $(window).on('load', this.setupLayout.bind(this));
        this.components = [];
    }


    getLayoutContainer(){
        return $('#app-layout');
    }

    createTab(){
        return {
            type: 'component',
            componentName: this.componentName
        };
    }

    getDefaultConfig(){
        return {
            settings: {
                showPopoutIcon: false,
                showMaximiseIcon: false,
            },
            content: [{
                type: 'row',
                content:[
                    this.createTab(),
                    this.createTab(),
                    this.createTab()
                ]
            }]
        };
    }


    loadSavedLayoutConfig(){
        const json = localStorage.getItem( this.stateSaveKey );
        if(json)
            return JSON.parse(json);
        else
            return null;
    }


    createLayout(){

        const forceNew = true;
        const savedState = this.loadSavedLayoutConfig();
        const currentConfig = savedState && !forceNew ? savedState : this.getDefaultConfig();

        const layout  = new GoldenLayout( currentConfig, this.getLayoutContainer() );
        layout.registerComponent( this.componentName, (container, componentState ) => {});
        layout.init();

        layout.on( 'stateChanged', () => {
            const state = JSON.stringify( this.layout.toConfig() );
            localStorage.setItem( this.stateSaveKey, state );
        });
        $(window).on('resize', () => {
            layout.updateSize();
            return false;
        });


        return layout;
    }

    spawnComponent (component, { title = name } = {}){
        const fake = $("<div></div>");
        this.layout.registerComponent( name, function( container, state ){
            container.getElement().html( component.getHtml()  );
        });
        const itemConfig = {
            type: 'component',
            componentName: component.name,
            componentState: {},
            title: title
        };
        this.layout.createDragSource( fake, itemConfig );
        fake.trigger('mousedown');
    }

    getComponentByName(name){
        return this.components.find(it => it.name === name)
    }


    registerComponent(component){
        this.components.push(component);
        this.layout.registerComponent( component.name, ( container ) => {
            container.getElement().html( component.getHtml()  );
        });
    }


    setupLayout(){
        this.layout = this.createLayout();

        // this.registerComponent(new Component("retard"));
        // this.spawnComponent(this.getComponentByName("retard"));
        // console.log(this.components)






    }
}