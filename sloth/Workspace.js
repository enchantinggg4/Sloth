import Bacon from 'baconjs';

export default class Workspace {
    constructor(){
        this.data = {};
        this.stream = Bacon.fromBinder(sink => this.update = sink);
        this.stream.onValue();
    }

    modify(newDataGen){
        let newData;
        if(typeof newData === 'function')
            newData = newDataGen(this.data);
        else
            newData = newDataGen;
        const copy = Object.assign({}, this.data);
        const data = Object.assign(copy, newData);
        this.update(data);
        this.data = data;
    }



    subscribe(onValue){
        return this.stream.onValue(onValue)
    }

}