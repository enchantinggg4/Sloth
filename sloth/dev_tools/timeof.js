export default (func) => {
    const time = new Date().getTime();
    const result = func();
    const endTime = new Date().getTime();
    return {
        start: time,
        end: endTime,
        duration: endTime - time,
        result: result
    };
}