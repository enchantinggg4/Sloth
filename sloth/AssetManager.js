import fs from 'fs';
export default class AssetManager{

    constructor(root){

    }



    getAsset(plugin, relativePath){
        return fs.readFileSync(`./plugins/${plugin}/${relativePath}`, { encoding: 'utf-8' })
    }

    getScript(plugin, relativePath){
        return require(`./plugins/${plugin}/${relativePath}`)
    }


}