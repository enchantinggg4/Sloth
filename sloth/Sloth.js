import AssetManager from './AssetManager'
import LayoutManager from './layout/LayoutManager'
import Store from './Store'
import Plugin from './Plugin'

import timeof from './dev_tools/timeof';

const fs = require('fs');
var path = require("path");

class Sloth{

    constructor(){
        this.assetManager = new AssetManager('./');
        this.store = new Store();
        this.layout = new LayoutManager("#app-layout");
        this.API = {
          // layout: this.layout.getPublicAPI()
        };
        this.plugins = [];
    }

    loadPlugins(){
        this.plugins = fs.readdirSync('./plugins').map(pluginName => {
            const plugin = new Plugin(pluginName);
            const config = JSON.parse(this.assetManager.getAsset(pluginName, 'plugin.json'));

            config.extensions.forEach(ext => {
                const ExtensionClass = require(`../plugins/calculator/${ext.main_app}`);
                const extension = new ExtensionClass(pluginName, ext, this);
                plugin.extensions.push(extension);
            });
            return plugin;
        });
    }

    applyPlugin(plugin){
        $(window).on('load', () => {
            plugin.extensions.forEach(extension => {
                extension.load();
            });
        })
    }

}

const app = new Sloth();
window.Sloth = app;
app.loadPlugins();
app.applyPlugin(app.plugins[0])