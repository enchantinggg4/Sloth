import Workspace from './Workspace';
export default class Store {

    constructor(){
        this.persistent = {};
        this.temp = {};
    }

    getPersistentPluginWorkspace(pluginName, struct = {}){
        if(!this.persistent[pluginName]) {
            this.persistent[pluginName] = new Workspace();
            this.persistent[pluginName].data = struct;
        }
        return this.persistent[pluginName];
    }


    getTemporaryPluginWorkspace(pluginName, struct = {}){
        if(!this.persistent[pluginName]) {
            this.persistent[pluginName] = new Workspace();
            this.temp[pluginName].data = struct;
        }
        return this.temp[pluginName];
    }

}